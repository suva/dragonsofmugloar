package com.bigbank.mugloar;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class Application {
	final static String url = "http://www.dragonsofmugloar.com/api/game";
	final static String putUrl = "http://www.dragonsofmugloar.com/api/game/{gameid}/solution";
	
    public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
    	ObjectMapper mapper = new ObjectMapper();
    	System.out.println("Getting data from a game API");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
         
        String result = restTemplate.getForObject(url, String.class);
        GameData gameData = mapper.readValue(result, GameData.class);
        System.out.println("Here is the game data:");
        System.out.println("GameId: " + gameData.getGameId() + "\n" +
        		"Knight name: " + gameData.getKnight().getName() + "\n" +
        		"Knight attack: " + gameData.getKnight().getAttack() + "\n" +
        		"Knight armor: " + gameData.getKnight().getArmor() + "\n" +
        		"Knight agility: " + gameData.getKnight().getAgility() + "\n" +
        		"Knight endurance: " + gameData.getKnight().getEndurance());
        
        Map<String, Integer> params = new HashMap<String, Integer>();
        params.put("gameid", gameData.getGameId());
        System.out.println("Preparing to battle");
        Dragon dragon = new Dragon(gameData.getKnight().getAttack(),
        		gameData.getKnight().getArmor(),
        		gameData.getKnight().getAgility(),
        		gameData.getKnight().getEndurance());

		Solution solution = new Solution();
		solution.setDragon(dragon);

    	HttpEntity<Solution> requestEntity = new HttpEntity<>(solution, headers);
    	System.out.println("Sending dragon to API");
    	ResponseEntity<SolutionResponse> response = restTemplate.exchange(putUrl, HttpMethod.PUT, requestEntity, SolutionResponse.class, params);
    	System.out.println("Battle result: " + response.getBody().getStatus());
    	System.out.println("Result message: " + response.getBody().getMessage());
    }
}