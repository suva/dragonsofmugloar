package com.bigbank.mugloar;

public class GameData {
	
	private Integer gameId;
	private Knight knight;
	
	public Integer getGameId() {
		return gameId;
	}
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	public Knight getKnight() {
		return knight;
	}
	public void setKnight(Knight knight) {
		this.knight = knight;
	}

}
