package com.bigbank.mugloar;

public class Solution {
    private Dragon dragon;

    public Dragon getDragon() {
        return dragon;
    }

    public void setDragon(Dragon dragon) {
        this.dragon = dragon;
    }
}
